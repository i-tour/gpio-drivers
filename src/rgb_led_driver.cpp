//
// Created by ultcrt on 2021/5/16.
//

#include "rgb_led_driver.h"
#include <JetsonGPIO.h>

using namespace GPIO;
using namespace std;

RGBLEDDriver::RGBLEDDriver(int pwm_pin, int r_pin, int g_pin, int b_pin, int target_freq, int target_step):
PWMDriver(pwm_pin, target_freq, target_step) {
    this->r_pin = r_pin;
    this->b_pin = b_pin;
    this->g_pin = g_pin;

    setmode(BOARD);
    setup(this->r_pin, OUT, LOW);
    setup(this->b_pin, OUT, LOW);
    setup(this->g_pin, OUT, LOW);
}

void RGBLEDDriver::rgb_switch(Color color) const {
    char rgb_code = char(~char(color));
    output(b_pin, rgb_code & 1);

    rgb_code >>= 1;
    output(g_pin, rgb_code & 1);

    rgb_code >>= 1;
    output(r_pin, rgb_code & 1);
}

RGBLEDDriver::~RGBLEDDriver() {
    cleanup(r_pin);
    cleanup(g_pin);
    cleanup(b_pin);
}
