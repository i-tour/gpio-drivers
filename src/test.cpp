//
// Created by ultcrt on 2021/5/23.
//

#include <iostream>
#include "pwm_driver.h"
#include "radar_driver.h"
#include "rgb_led_driver.h"

using namespace std;

int main(int argc, char** argv) {
    RGBLEDDriver rgb(18, 37, 38, 40, 1000, 5);
    PWMDriver buzzer(13, 1000, 5);
    PWMDriver vibrator(15, 1000, 5);
    RadarDriver radar;

    while (true) {
        double dist = radar.get();
        cout << dist << endl;
        if (dist < 30) {
            rgb.stop();
            rgb.rgb_switch(Color::red);
            rgb.run();

            buzzer.run();
            vibrator.run();
        }
        else {
            rgb.stop();
            rgb.rgb_switch(Color::black);

            buzzer.stop();
            vibrator.stop();
        }
    }
}