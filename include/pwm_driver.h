//
// Created by ultcrt on 2021/4/27.
//

#ifndef GPIO_DRIVER_PWM_DRIVER_H
#define GPIO_DRIVER_PWM_DRIVER_H

#include <JetsonGPIO.h>

class PWMDriver {
private:
    int pwm_pin;
    int freq;
    int step;
    bool keep_running = false;
    void pwm_thread();
    GPIO::PWM* pwm;

public:
    explicit PWMDriver(int pin, int target_freq=1000, int target_step=5);
    void set_freq(int target_freq);
    void set_step(int target_step);
    void run();
    void stop();
    ~PWMDriver();
};


#endif //GPIO_DRIVER_PWM_DRIVER_H
