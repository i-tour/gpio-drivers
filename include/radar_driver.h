//
// Created by ultcrt on 2021/4/27.
//

#ifndef GPIO_DRIVER_RADAR_DRIVER_H
#define GPIO_DRIVER_RADAR_DRIVER_H

#include <future>

#define RX_A 7
#define TX_A 11
#define RX_B 16
#define TX_B 22
// Sonic speed m/ns
#define SONIC_SPEED 3.4E-7

class RadarDriver {
private:
    void detect(int rx, int tx, std::promise<double>& ret);
public:
    RadarDriver();
    double get();
    ~RadarDriver();
};


#endif //GPIO_DRIVER_RADAR_DRIVER_H
