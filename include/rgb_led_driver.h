//
// Created by ultcrt on 2021/5/16.
//

#ifndef GPIO_DRIVER_RGB_LED_DRIVER_H
#define GPIO_DRIVER_RGB_LED_DRIVER_H

#include "pwm_driver.h"

enum class Color: char{
    black=0,
    red=4,
    green=2,
    blue=1,
    yellow=6,
    purple=5,
    cyan=3,
    white=7
};

class RGBLEDDriver: public PWMDriver {
private:
    int r_pin;
    int g_pin;
    int b_pin;

public:
    RGBLEDDriver(int pwm_pin, int r_pin, int g_pin, int b_pin, int target_freq, int target_step);
    void rgb_switch(Color color) const;
    ~RGBLEDDriver();
};


#endif //GPIO_DRIVER_RGB_LED_DRIVER_H
